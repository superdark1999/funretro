import { Fragment } from "react";
import { Switch, Route } from "react-router-dom";
import Navbar from "./Component/Navbar/Navbar";
import Board from "./Component/Board/Board";
import "./App.css";
import DashBoard from "./Component/DashBoard/DashBoard";

function App() {
  return (
    <Fragment>
      <Navbar />
      <Switch>
        <Route path="/dashboard" component={DashBoard} />
        <Route path="/board" component={Board} />
      </Switch>
    </Fragment>
  );
}

export default App;
