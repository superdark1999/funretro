import firebase from "../firebase";

const collectionName = "item";

async function getAll() {
  const db = firebase.firestore();
  const data = await db.collection(collectionName).get();

  return data.docs.map((item) => ({ ...item.data(), docId: item.id }));
}

async function Create(item) {
  const db = firebase.firestore();
  await db.collection(collectionName).get();
}

async function Update(item) {
  const itemStored = { ...item };
  delete itemStored.docId;
  const db = firebase.firestore();
  await db.collection(collectionName).doc(item.docId).set(itemStored);
}

async function Delete(item) {
  const db = firebase.firestore();
  await db.collection(collectionName).doc(item.docId).delete();
}

export default {
  getAll,
  Create,
  Update,
  Delete,
};
