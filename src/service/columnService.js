import firebase from "../firebase";

// const db = firebase.ref("/item");
const collectionName = "column";

async function getAll() {
  const db = firebase.firestore();
  const data = await db.collection(collectionName).get();

  return data.docs.map((item) => ({ ...item.data(), docId: item.id }));
}

export default {
  getAll: getAll,
};
