import firebase from "firebase";

let firebaseConfig = {
  apiKey: "AIzaSyC05UXzvuXUO65QdB7yUH8-ZYpyg_JKNGs",
  authDomain: "funretro-a95fc.firebaseapp.com",
  databaseURL: "https://funretro-a95fc.firebaseio.com",
  projectId: "funretro-a95fc",
  storageBucket: "funretro-a95fc.appspot.com",
  messagingSenderId: "1042108391685",
  appId: "1:1042108391685:web:6c03bc0e981ce87efd176c",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase;
