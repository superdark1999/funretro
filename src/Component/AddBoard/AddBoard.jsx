import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";

function AddBoard(props) {
  return (
    <li className="board-item add-item">
      <span className="add">
        <div className="add-item-btn">
          <FontAwesomeIcon icon={faPlus} />
        </div>
        <small>Add Board</small>
      </span>
    </li>
  );
}

export default AddBoard;
