import React from "react";
import ItemDone from "./ItemDone/ItemDone";
import "./Item.css";
import ItemEdit from "./ItemEdit/ItemEdit";

function Item({ color, item }) {
  return item.edit ? (
    <ItemEdit item={item} color={color} />
  ) : (
    <ItemDone item={item} color={color} />
  );
}

export default Item;
