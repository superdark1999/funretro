import React, { useContext } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSmile, faTrash } from "@fortawesome/free-solid-svg-icons";
import ItemContext from "../../Context/itemContext";

function ItemEdit({ item, color }) {
  const itemContext = useContext(ItemContext);

  return (
    <div className="item-edit" style={{ borderColor: color }}>
      <div className="item-edit-header">
        <FontAwesomeIcon icon={faSmile} className="icon-feel icon" />
        <textarea
          name="content"
          className="item-input"
          value={item.content}
          onChange={(e) => itemContext.handleChange(e, item)}
        ></textarea>
      </div>
      <div className="item-control">
        <button onClick={() => itemContext.handleChangeStatusItem(item)}>
          Done
        </button>
        <FontAwesomeIcon
          icon={faTrash}
          className="icon"
          onClick={() => itemContext.handleDeleteItem(item)}
        />
      </div>
    </div>
  );
}

export default ItemEdit;
