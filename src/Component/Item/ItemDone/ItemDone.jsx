import React, { useContext } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPen,
  faComment,
  faThumbsUp,
} from "@fortawesome/free-solid-svg-icons";
import ItemContext from "../../Context/itemContext";

function ItemDone({ item, color }) {
  const itemContext = useContext(ItemContext);

  return (
    <div className="item-done" style={{ backgroundColor: color }}>
      <div className="item-header">
        <span className="item-content">{item.content}</span>
        <FontAwesomeIcon
          icon={faPen}
          className="icon-edit icon"
          onClick={() => itemContext.handleChangeStatusItem(item)}
        />
      </div>
      <div className="item-social">
        <FontAwesomeIcon icon={faThumbsUp} className="like icon" />
        <FontAwesomeIcon icon={faComment} className="comment icon" />
      </div>
    </div>
  );
}

export default ItemDone;
