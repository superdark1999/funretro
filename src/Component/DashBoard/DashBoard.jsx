import React, { useContext, useState } from "react";
import AddBoard from "../AddBoard/AddBoard";
import BoardItem from "../BoardItem/BoardItem";

function DashBoard(props) {
  const [boards, setBoards] = useState([
    {
      id: 1,
      name: "React-UI",
      date: "19/11/2020",
      numberOfCard: 4,
    },
    {
      id: 2,
      name: "Project funretro",
      date: "15/10/2020",
      numberOfCard: 5,
    },
  ]);

  return (
    <div className="main">
      <ul className="list-board-item">
        <AddBoard />
        {boards.map((board) => (
          <BoardItem
            name={board.name}
            numberOfCard={board.numberOfCard}
            date={board.date}
          />
        ))}
      </ul>
    </div>
  );
}

export default DashBoard;
