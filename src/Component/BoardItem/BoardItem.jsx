import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClock } from "@fortawesome/free-solid-svg-icons";
import "../../App.css";

function BoardItem({ name, numberOfCard, date }) {
  return (
    <li className="board-item dashboard-small">
      <div className="dashboard-item-body">
        <p className="board-name">{name}</p>
        <span className="board-date">
          <FontAwesomeIcon icon={faClock} className="icon-clock" />
          <span>{date}</span>
        </span>
        <span className="board-number-card">{numberOfCard} card</span>
      </div>
    </li>
  );
}

export default BoardItem;
