import React from 'react';
import Control from '../Control/Control';
import Filter from '../Filter/Filter';
import "./Navbar.css";

function Navbar(props) {
  return (
    <div>
      <header className="app-header">
        <Filter />
        <Control />
      </header>
    </div>
  );
}

export default Navbar;