import React, { useContext } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSquare, faPlus } from "@fortawesome/free-solid-svg-icons";
import { Droppable, Draggable } from "react-beautiful-dnd";
import Item from "../Item/Item";
import ItemContext from "../Context/itemContext";
import "./Column.css";

function Column({ column, onAddNewItem }) {
  const { items, handleDragStart, handleDragOver, handleDragEnd } = useContext(
    ItemContext
  );

  const itemsOfColumn = items.filter((item) => item.columnId === column.id);

  itemsOfColumn.sort((a, b) => {
    return a.order < b.order ? 1 : -1;
  });

  return (
    <div className="board-column">
      <div className="title">
        <FontAwesomeIcon
          icon={faSquare}
          className="square-icon"
          style={{ color: column.color }}
        />
        <span>{column.name}</span>
      </div>

      <button className="add-button" onClick={() => onAddNewItem(column.id)}>
        <FontAwesomeIcon icon={faPlus} />
      </button>

      <Droppable droppableId={column.id}>
        {(provided, snapshot) => (
          <div
            className="drop-space"
            ref={provided.innerRef}
            {...provided.droppableProps}
          >
            {itemsOfColumn.map((item, index) => (
              <Draggable draggableId={item.id} index={index} key={item.id}>
                {(provided, snapshot) => (
                  <div
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    className="drag-item"
                  >
                    <Item color={column.color} item={item} />
                  </div>
                )}
              </Draggable>
            ))}
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </div>
  );
}

export default Column;
