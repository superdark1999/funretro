import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar, faUserCircle } from "@fortawesome/free-solid-svg-icons";
import "./Control.css";

function Control(props) {
  return (
    <div className="header-control">
      <span className="prime">
       <FontAwesomeIcon icon={faStar} className="icon-star"/>
       <span>Prime Dicretive</span>
       <FontAwesomeIcon icon={faUserCircle} className="icon-profile"/>
      </span>
    </div>
  );
}

export default Control;