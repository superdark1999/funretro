import React, { useState } from "react";
import { DragDropContext } from "react-beautiful-dnd";
import Column from "../Column/Column";
import ColumnContext from "../Context/columnContext";
import ItemContext from "../Context/itemContext";
import columnService from "../../service/columnService";
import itemService from "../../service/itemService";
import "./Board.css";

function Board(props) {
  const [columns, setColumns] = useState([]);

  const [items, setItems] = useState([]);

  React.useEffect(() => {
    const fetData = async () => {
      const items = await itemService.getAll();
      const columns = await columnService.getAll();

      setColumns(columns);
      setItems(items);
    };
    console.log("init data");
    fetData();
  }, []);

  const handleChangeStatusItem = async (item) => {
    const cloneItems = [...items];
    const index = cloneItems.indexOf(item);
    cloneItems[index] = { ...item };
    cloneItems[index].edit = !cloneItems[index].edit;

    setItems(cloneItems);
    await itemService.Update(cloneItems[index]);
  };

  const handleChange = (e, item) => {
    const cloneItems = [...items];
    const index = cloneItems.indexOf(item);
    cloneItems[index] = { ...item };
    cloneItems[index].content = e.target.value;

    setItems(cloneItems);
  };

  const handleAddEditItem = (columnId) => {
    const cloneItems = [...items];
    const maxId = Math.max(...cloneItems.map((item) => item.id));
    const maxOrder = Math.max(
      ...cloneItems.map((item) => (item.columnId === columnId ? item.order : 0))
    );
    const newItem = {
      id: `${maxId + 1}`,
      content: "",
      edit: true,
      columnId: `${columnId}`,
      order: maxOrder + 1,
    };
    cloneItems.push(newItem);
    setItems(cloneItems);
  };

  const handleDeleteItem = async (item) => {
    const cloneItems = items.filter((i) => i.id !== item.id);
    setItems(cloneItems);

    await itemService.Delete(item);
  };

  const onDragUpdate = async ({ destination, draggableId }) => {
    console.log("Drag update:", destination, draggableId);
    if (!destination) return;
    const cloneItems = [...items];
    const index = cloneItems.findIndex((item) => item.id === draggableId);
    const cloneDraggedItem = { ...cloneItems[index] };
    const maxOrderInDestination = Math.max.apply(
      Math,
      cloneItems.map((item) =>
        item.columnId === destination.droppableId ? item.order : 0
      )
    );
    cloneDraggedItem.columnId = destination.droppableId;
    cloneDraggedItem.order = maxOrderInDestination - destination.index + 1;
    // 2 = 3 - 2
    cloneItems[index] = cloneDraggedItem;
    setItems(cloneItems);
    await itemService.Update(cloneItems[index]);
  };
  const onDragEnd = () => {};

  return (
    <DragDropContext onDragEnd={onDragEnd} onDragUpdate={onDragUpdate}>
      <div className="board">
        <ColumnContext.Provider value={columns}>
          <ItemContext.Provider
            value={{
              items: items,
              handleChange: handleChange,
              handleChangeStatusItem: handleChangeStatusItem,
              handleDeleteItem: handleDeleteItem,
            }}
          >
            {columns.map((column) => (
              <Column
                key={column.id}
                column={column}
                onAddNewItem={handleAddEditItem}
              />
            ))}
          </ItemContext.Provider>
        </ColumnContext.Provider>
      </div>
    </DragDropContext>
  );
}

export default Board;
