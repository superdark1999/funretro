import React from 'react';
import './Filter.css';

function Filter(props) {
  return (
    <div className="header-filter">
      <span className="logo">FunRetro</span>
      <input className="search" />
    </div>
  );
}

export default Filter;